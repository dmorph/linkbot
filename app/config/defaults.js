// App defaults
const path = require('path');

module.exports = {
	config: {
		serverUrl: "http://127.0.0.1:3000",
		trayWaitIcon: 'img/pbs-wait.png',
		trayIcon: 'img/pbs.png',
		modules: ['glf', 'slf'],
		appTitle: 'AppLinkage Client'
	},
	glf: {
		name: 'glf',
		socketPort: 9002,
    dbConnection: {
      user: 'test',
      password: 'test',
      server: 'localhost',
      database: 'Test'
    }
	},
	slf: {
		name: 'slf',
		socketPort: 9001
	}
}
