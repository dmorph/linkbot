'use strict';

const _ = require('lodash');
const Promise = require('bluebird');
const ip = require('ip');
// st: Pathway
const execProcess = require('child_process').execFile;
// end: Pathway

// const configUtils = require('./config-utils');
const {log, inspect} = require('../logger');
// ?? config: configConsts, events: eventConsts,
const {modules: moduleConsts} = require('./defaults');

let appConfig;

module.exports = {
	setConfig: (config) => {
		appConfig = config;
		log.silly("baseModules factory config set");
	},
	baseModule: {
		params: {},
	  // Return modules consts for convenience
		consts: {},
	  // consts() {
	  //   return moduleConsts[this.params.ModuleClass];
	  // },
		setContext(config, globalContext, moduleContext) {
			// Merge input link data with fields from previous step/module into module params
			_.merge(this.params, globalContext, moduleContext);
			// Merge current modules with modules config and consts
			getModuleConfigParams(config, this.params);
			log.silly(`- module ${this.params.ModuleClass} params: ${inspect(this.params)}`);
			_.merge(this.consts, moduleConsts[config.ModuleClass]);
			log.silly(`- *module ${this.params.ModuleClass} consts: ${inspect(moduleConsts)}`);
		},
		// Default impl
		exec() { return new Promise.resolve() }
	},
	GetExponareIDs: {
		exec() {
			// Retrieve the selected id's and store as the property nomintated by 'override.selectionidds'
			return new Promise( (resolve, reject) => {
				log.debug('GetExponareIDs.exec');
				// // resolve(
				// 	// configUtils.toJson(this.params.xmlfilepathexp).then( (linkFileData) => {
	      // log.silly(`this.params: ${inspect(this.params)}`);
	      // // let paramConsts = this.params.consts;
				// log.debug(`*** consts: ${this.consts.propOverrideIds}`);
	      let propOverride = this.consts.propOverrideIds;
	      let propName = this.consts.propSelectedIds;
	      let ids = getLinkDetailsSelectedIds(this.params, propName);
	      if (this.params[propOverride]) {
	      	propName = this.params[propOverride];
	      }

	      let delimOutput = ids.join(this.params[this.consts.propDelimiter]);
	      log.debug(`seletectId: ${propName}=${delimOutput}`);
	      resolve({
	        [propName]: delimOutput
	      });
			});
		}
	},
  GetEnvironmentVariable: {
	  exec() {
	    return new Promise( (resolve, reject) => {
				log.debug('GetEnvironmentVariable.exec');
	      let envName = process.env[this.consts.propEnvName];
	      if (envName.toLowerCase() === 'programfiles') {
	        if (isWindows64()) {
	          envName = 'programfiles(x86)';
	        }
	        else {
	          envName = 'programfiles';
	        }
	      }

	      let envValue = process.env[envName];
	      resolve({
	        [this.consts.propOutName]: envValue
	      });
	    });
	  }
	},
	GetIPAddress: {
	  exec() {
	    return new Promise( (resolve, reject) => {
				log.debug('GetIPAddress.exec');
	      resolve({
	        [this.consts.propIPAddress]: ip.address()
	      });
	    });
	  }
	},
	GetUserName: {
	  exec() {
	    return new Promise( (resolve, rejectd) => {
				log.debug('GetUserName.exec');
	      resolve({
	        [this.consts.propUid]: process.env.USERNAME,
	        [this.consts.propDomain]: process.env.USERDOMAIN
	      });
	    });
	  }
	},
	GlobalProperties: {
	  setContext(config, globalContext, moduleContext) {
	    // Place module properties into the globalContext
	    log.debug('exec GlobalProperties.context');
	    getModuleConfigParams(config, globalContext);
	  }
	},
	RegExReplace: {
	  exec() {
	    return new Promise( (resolve, reject) => {
				log.debug('RegExReplace.exec');
	      let params = this.params;
	      let consts = this.consts;
	      let input = params[consts.inputstring];
	      let output = input.replace(params[consts.propSearch], params[consts.propReplace]);
	      resolve({
	        [consts.propOutput]: output
	      });
	    });
	  }
	},
	WriteXML: {
		// TODO: Still required now??
		exec() {
			return new Promise( (resolve, reject) => {
				log.warn('Using deprecated module: WriteXML.exec');
				// let paramConsts = consts.WriteXML;
				let params = this.params;
				let output = {
					LinkDetails : {
						LinkId: params.linkname,
						ActiveSelectionId: '',
						SelectionIds : {
							SelectionId : [params.data]
						}
					}
				}
				log.silly(`xml output: ${inspect(output)}`);
				// configUtils.toXml(params.xmlfilepath, output);
			});
		}
	},
	PathwayLinkOut: {
		exec() {
			return new Promise( (resolve, reject) => {
				log.debug('PathwayLinkOut.exec');
				let params = this.params;
	      let staticParams = getPathwayStaticParams(params, this.consts.propStaticArgs);
				// com /giscommand command=GIS,COMGIS,DISP,,PARC,,N,013/9//7874
				let cmd = `command=${params.interface},${params.product},${params.command},,${params.level},,N,${params.data}`
				let cmdArgs = ['/giscommand', cmd]
				log.debug(`path: ${params.path}, args: ${cmdArgs}`);
	      log.debug('Performing command')
				execProcess(params.path, cmdArgs, (err, stdout, stderr) => {
					log.silly("stdout:" + stdout);
	        if(err) {
	          log.error("error:" + err);
	        }
	        if (stderr) {
	          log.warn("stderr: " + stderr);
	        }
				});
	      resolve();
			});
		}
	}
}

function getPathwayStaticParams(moduleParams, propStaticArgs) {
  let staticArgs = moduleParams[propStaticArgs];
	log.debug(`staticArgs > [${staticArgs}]=${inspect(moduleParams)}`);
  staticArgs.forEach( (arg) => {
    let pair = arg.split('=')
    if (pair.length == 2) {
      moduleParams[pair[0]] = pair[1];
    }
  });
}

function getModuleConfigParams(config, params) {
  // Push module's configured params are stored in params (the task/job's variables).
  // A template params is parsed before being pushed
	config.Params.forEach( (paramConfig) => {
		let paramVal = paramConfig.ParamValue;
		paramVal = parseTemplateParam(params, paramVal);
		params[paramConfig.ParamName] = paramVal;
	});

  // let modClass = config.ModuleClass;
	// log.silly(`${modClass} params: ${inspect(params)}`);
  // log.silly(`${modClass} consts: ${inspect(moduleConsts[modClass])}`);
	return params;
}

function parseTemplateParam(params, paramVal) {
	let rgxTemplateParam = appConfig.rgxTemplateParam;
	if (rgxTemplateParam.test(paramVal)) {
		var regMatch = paramVal.match(rgxTemplateParam);
		let templateParamName = regMatch[1];
		let templateParamVal = params[templateParamName];
		paramVal = paramVal.replace(templateParamName, templateParamVal).replace(/~/g,'');
	}
	return paramVal;
}

function getLinkDetailsSelectedIds(linkData, propName) {
	let inputIds = linkData[propName];
	let selectedIds = [];
	if (Array.isArray(inputIds)) {
		selectedIds = inputIds;
	}
	else {
		selectedIds = [inputIds];
	}
	log.debug(`selectedIds: ${selectedIds}`);
	return selectedIds;
}

function isWindows64() {
  // Second section is only relevant for node-webkit app, using the pre-built binary
  return process.arch === 'x64' || process.env.hasOwnProperty('PROCESSOR_ARCHITEW6432');
}
