'use strict';

// TODO: repeated toJson && toXml
const fs = require('fs');
const path = require('path');
const xml2js = require('xml2js');
const promise = require('bluebird');
const readFileAsync = promise.promisify(fs.readFile);
const parseStringAsync = promise.promisify(xml2js.parseString);
const parseConfigAsync = promise.promisify(parseConfig);
// TODO: end

const _ = require('lodash');
const {log, inspect} = require('../logger');

module.exports = {
	toXml, toJson, parseConfig
}

// TODO: repeated toJson && toXml
function toJson(fileName) {
	let filePath = getPath(fileName);
	return readFileAsync(filePath, "utf-8")
			.then( (text) => {
				return parseStringAsync(text, {explicitArray: false})
			});
}

function toXml(fileName, obj) {
	// let filePath = getPath(fileName);
	let filePath = getPath('./pathOutput.xml');
	console.log(`xmlOutput: ${filePath}`);
	let xmlBuilder = new xml2js.Builder();
	let xml = xmlBuilder.buildObject(obj);
	fs.writeFile(filePath, xml);
}

function getPath(fileName) {
	return path.normalize(path.join(__dirname, fileName));
}
// TODO: end

function parseConfig(config) {
	return new Promise((resolve, reject) => {
		let procsParams={}, procs={}, appLinks={};

		log.debug('Parsing GLF json config');
		log.silly(`Raw json: ${inspObj(config)}`);
		// Parse proc params
		let procParams = config.LinkProcessBase.ProcessModuleParam
		procParams.forEach( (elem) => {
			let param = parseObj(elem);
			if (procsParams[param.ProcessId]) {
				procsParams[param.ProcessId].push(param);
			}
			else {
		 		procsParams[param.ProcessId] = [param];
			}
			delete param.ProcessId;
		});

		// Parse link procs
		let linkProcess = config.LinkProcessBase.LinkProcess;
		linkProcess.forEach( (elem) => {
			let linkProc = parseObj(elem)
			if (procs[linkProc.LinkId]) {
				procs[linkProc.LinkId].push(linkProc);
			}
			else {
				procs[linkProc.LinkId] = [linkProc];
			}
			delete linkProc.LinkId;
			linkProc.Params = procsParams[linkProc.ProcessId];
		});

		// Parse LinkDefinitions
		let linkDefs = config.LinkProcessBase.LinkDef;

		linkDefs.forEach( function(elem) {
			let linkDef = parseObj(elem)
			linkDef['LinkProcess'] = procs[linkDef.LinkId];
			linkDef['LinkProcess'].sort(comareParams);
			appLinks[linkDef.LinkName] = linkDef;
			delete linkDef.LinkName;
		});

    log.debug('Converted GLF xml config into json');
    resolve(appLinks);
	});
}

// TODO: if this is removed, glf fails but glf-xml-config passes??
function comareParams(a,b) {
  if (a.Order < b.Order)
    return -1;
  if (a.Order > b.Order)
    return 1;
  return 0;
}

// Deprecated
function getPath(fileName) {
	return path.normalize(path.join(__dirname, fileName));
}

function parseObj(parent) {
	let obj = {}
	_.forEach(parent, (elem, key) => {
		obj[key] = parseVal(elem);
	});
	return obj;
}

// xml2js defaults values to array
function parseVal(elem) {
	return elem[0]
}