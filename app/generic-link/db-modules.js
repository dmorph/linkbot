'use strict';

const sql = require('mssql');

const {log, inspect} = require('../logger');
// const {modules: moduleConsts} = require('./defaults');

let moduleConfig;

module.exports = DbManager;

function DbManager(config) {
  this.moduleConfig = config;
}

DbManager.prototype = {
  DB_Delete,
  DB_Insert,
  DB_Insert_MultipleList,
  DB_Read
}

function DB_Delete(queryConfig, moduleConsts) {
  return executeQuery(queryConfig, moduleConsts);
}

function DB_Insert(queryConfig, moduleConsts) {
  // return executeQuery
}

function DB_Insert_MultipleList(queryConfig, moduleConsts) {   // ExpCoreLinks
  // new Promise(resolve, reject) {

  // };
}

function DB_Read(queryConfig, moduleConsts) {
  return executeQuery(queryConfig, moduleConsts).then( (data) => {
    log.debug('DB_Read eset keys');
  });
}

function executeQuery(queryConfig, crudConsts) {
  let connStr = queryConfig[crudConsts.propConn];
  log.debug(`linkInput.connStr: ${connStr}`);
  let sqlQuery = queryConfig[crudConsts.propSql];
  log.debug(`linkInput.sqlQuery: ${connStr}`);

  return getConnection(connStr).then( () => {
    let request = sql.Request()
    return request.query(sqlQuery).then( (data) => {
      data[config.propAffectCnt] = request.rowsAffected;
      log.debug(`execQuery result: ${inspect(data)}`);
      return data;
    });
  });
}

function getConnection(connConf) {
  let config;
  if(queryConf) {
    config = queryConf;
  }
  else {
    config = moduleConfig.dbConnection;
  }
  log.debug(`Created conn: ${config}`);
  return sql.connect(config);
}