'use strict'

// Module constants
module.exports = {
  config: {
	socketPort: 3011,
	rgxTemplateParam: /~(.*)~/,
    dbConnection: {
      user: 'test',
      password: 'test',
      server: 'localhost',
      database: 'Test',
      pool: {
        max: 10,
        min: 0,
        idleTimeoutMillis: 300
      }
	}
  },
  events : {
	invalidConfig: "invalid-config",
	invalidLink: "invalid-link",
	executeSuccess: "exec-success",
	executeFailure: "exec-failure",
	glflinkIn: "glf-linkin",
	slfLinkIn: "slf-linkin",
	server: {
	  started: "server-started",
	  linkin: "ssa-linkin"
	}
  },
  modules: {
    DB_Delete: {
      propConn: 'connectionstringdel',
      propSql: 'sqldelete',
      propAffectCnt: 'numdeletesok'
    },
	DB_Insert: {
      propConn: 'connectionstringins',
      propSql: 'sqlinsert',
      propInsertList: 'sqlinsertlist',
      propItemReplacing: 'selectionid',
      propAffectedCnt: 'numinsertsok'
    },
    DB_Insert_MultipleList: {
      propConn: 'connectionstringins',
      propSql: 'sqlinsert',
      propInsertList: 'sqlinsertlist',
      propItemReplacing: 'selectionid',
      propAffectedCnt: 'numinsertsok'
    },
    DB_Read: {
      propConn: 'connectionstringread',
      propSql: 'sqlquery',
      propOutputPrefix: 'dbread_col'
    },
    GetExponareIDs: {
	  propSelectedIds: 'selectionIds',
	  propOverrideIds: 'override.selectionids',
	  propDelimiter: 'delimiterexp'
	},
    GetIPAddress: {
      propIPAddress: "ipaddress"
    },
    GetPCIdentifier: {
      propForceIP: 'forceuseipaddress',
      propOutput: 'pcidentifier'
    },
    GetEnvironmentVariable: {
      propEnvName: "environmentvariable",
      propOutName: "environmentvariablevalue"
    },
    GetUserName: {
      propUid: 'username',
      propDomain: 'domain'
    },
    PathwayLinkOut: {
      propStaticArgs: 'staticArgs'
    },
    RegExReplace: {
      propInput: 'inputstring',
      propSearch: 'searchexpression',
      propReplace: 'replaceexpression',
      propOutput: 'outputstring'
    },
    RunApplication: {
      propPath: 'path',
      propArgs: 'args'
    },
    Show_DialogAsk: {
      propPrompt: 'prompttexttask',
      propTitle: 'titletextask',
      propOutput: 'dialogreturnvalueask'
    },
    Show_DialogMultiListbox: {
      propDialogTitle: 'titletextmlb',
      propDialogPrompt: 'prompttextmlb',
      propOkLabel: 'okktextmlb',
      propCancleLabel: 'canceltextmlb',
      propList: 'textdelimlistmlb',
      propValueName: 'valuedelimlistmlb',
      propOutputName: 'dialogreturnvaluemlb',
      propSkipLabel: 'skiptextmlb'
    },
    WriteDelimitedFile: {
      propFileName: 'filename',
      propFileDelim: 'filedelimiter',
      propList: 'filedelimitedlist',
      propListDelim: 'listdelimiter',
      propOutput: 'filename'
    },
	WriteXML: {
  	  propLinkId: "LinkId",
  	  propActiveId: "ActiveSelectionId"
	}
  }
}
