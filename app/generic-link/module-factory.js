'use strict';

const _ = require('lodash');
const baseModules = require('./base-modules');
// const {DB_Delete,DB_Insert,DB_Insert_MultipleList,DB_Read}
// const DbManager = require('./db-modules');

const {log, inspect} = require('../logger');
let appConfig;
let factoryModules = {
  // DB_Delete,
  // DB_Read
};

_.merge(factoryModules, baseModules);
module.exports = ModuleFactory;

var Modules = {
  baseModule: true,
  // DB_Delete,
  // // DB_Insert
  // // DB_Insert_MultipleList,
  // DB_Read,
  GetExponareIDs: true,
  GetEnvironmentVariable: true,    // ExpCoreLinks
  GetIPAddress: true,
  // GetPCIdentifier,   // ExpCoreLinks
  GetUserName: true,
  GlobalProperties: true,
  PathwayLinkOut: true,
  RegExReplace: true,
  // RunApplicationWithList
  // RunApplication
  // Show_DialogAsk   // ExpCoreLinks
  // Show_DialogMultiListbox,
  // WriteDelimitedFile,
  WriteXML: true
}

function ModuleFactory(config, moduleConfig, globalContext, stepContext) {
  setConfig(config);

  let moduleName = moduleConfig.ModuleClass;
  let factoryModule = factoryModules[moduleName];
  if (!factoryModule) {
    throw new Error(`GLF Module "${moduleName}" was not found!`);
  }

  log.debug(`ModuleFactory creating ${moduleName}`);
  let taskModule = {};
  _.merge(taskModule, factoryModules.baseModule, factoryModule);
  taskModule.setContext(moduleConfig, globalContext, stepContext);
  return taskModule;
}

function setConfig(config) {
  log.silly('ModuleFactory setting config');
  appConfig = config;
  factoryModules.setConfig(config);
}
// const {modules: moduleConsts} = require('./defaults');
// var DB_Delete = {
//   exec() {
//     let dbManager = new DbManager();
//     let modConsts = _.merge({}, this.consts, appConfig.dbConnection);
//     return dbManager.DB_Delete(this.params, modConsts);
//   }
// }
//
// var DB_Read = {
//   exec() {
//     let dbManager = new DbManager();
//     let modConsts = _.merge({}, this.consts, appConfig.dbConnection);
//     return dbManager.DB_Read(this.params, modConsts);
//   }
// }
