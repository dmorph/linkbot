'use strict';

const util = require("util");
const EventEmitter = require("events").EventEmitter;
const express = require('express');
const io = require('socket.io');
const _ = require('lodash');
const Promise = require('bluebird');
const initConfigAsync = Promise.promisify(executeModule);
// ??
const exec = require('child_process').execFile;
// ??

const ModuleFactory = require('./module-factory.js');
const configUtils = require('./config-utils');
const {log, inspect} = require('../logger');
// ?? moduleConsts
const {config: configConsts, events: eventConsts, modules: moduleConsts} = require('./defaults');

let appConfig, isValidConfig;
let expressServer, socketServer, socketCnt;

module.exports = GlfProcessor;

function GlfProcessor(config) {
  EventEmitter.call(this);
  updateConfig(config);
}

util.inherits(GlfProcessor, EventEmitter);

GlfProcessor.prototype.init = init;
GlfProcessor.prototype.updateConfig = updateConfig;
// GlfProcessor.prototype.importXml = importXml;
GlfProcessor.prototype.executeModule = executeModule;
// GlfProcessor.prototype.sendInputLink = triggerInputEvent;
GlfProcessor.prototype.eventConsts = eventConsts;

function init() {
  log.debug('Sarting glf link server');
	let port = appConfig.socketPort;
  expressServer = express()
      .use((req, res) => res.sendFile(indexHtml))
      .listen(port, () => log.info(`GLF Links started listening on ${port}`));
  socketServer = io(expressServer);
  socketServer.sockets.on('connection', (socket) => {
  	log.debug(`Glf socket: new connection #${++socketCnt}`);
  	socket.on(eventConsts.server.linkIn, (data) => {
  		log.debug(`Glf socket: ${eventConsts.server.linkIn} event`);
  		log.silly(`Glf sending Slf link in: ${data}`);
  		this.emit(eventConsts.slfLinkIn, data);
  	});
  	socket.on('disconnect', () => {
  		log.debug(`Glf socket: disconnect #${--socketCnt}`);
  	});
  });

  this.emit(eventConsts.server.started);
}

function updateConfig(config) {
  log.info("Updating GLF config");
  appConfig = _.merge({}, configConsts, config);
  log.silly(`Updated GLF config: ${inspect(config)}`);
  isValidConfig = checkConfig();
  if (isValidConfig) {
    log.debug(`Set app port: ${appConfig.socketPort}, def port: ${configConsts.socketPort}`);
  }
}

function checkConfig() {
  let valid = true;
  if ( Object.keys(appConfig) === 0 || !appConfig.socketPort) {
  	log.warn("Configuration is invalid");
  	valid = false;
  }
  return valid;
}

function executeModule(linkEvent) {
  if (!isValidConfig) {
    log.debug('GLF module exec: invalid config');
    this.emit(eventConsts.invalidConfig);
    return;
  }

  let linkName = linkEvent.linkName;
  let linkConfig = appConfig[linkName];
  if (!linkConfig) {
    log.error(`GLF LinkProcess def could not be found: ${linkName}`);
    this.emit(eventConsts.invalidConfig);
    return;
  }

  log.silly(`linkName: ${linkName} with config: ${inspect(linkConfig)}`);
  // log.debug(`appCongif: ${inspect(appConfig)}`);
  log.info(`Executing module: ${linkName}`);
  runTasks(linkConfig, linkEvent);
}

function runTasks(linkConfig, globalContext) {
	log.debug(`runModule LinkProcess "${globalContext.linkName}"" found id=${linkConfig.LinkId}`); // TODO: Const
	let taskRunner;
	linkConfig.LinkProcess.forEach( (moduleConfig, cntr) => {
		if (cntr == 0) {
			taskRunner = createTaskAync(moduleConfig, globalContext);
		}
		else {
			if (moduleConfig.ModuleClass == 'WriteXML') {
				log.info('writexml check!!');
			}
			taskRunner = taskRunner.then( (stepContext) => {
        // log.silly(`XXXXXXXXXXXXXX: ${inspect(this.configConsts)}`)
				return createTaskAync(moduleConfig, globalContext, stepContext);
			});
		}
	});

	taskRunner.then(taskSuccess).catch(taskFailed);
}

let mxCnt=0;
function createTaskAync(moduleConfig, globalContext, stepContext={}) {
  log.debug(`Generating task: ${moduleConfig.ModuleClass}`);  // TODO: Const
	let module = ModuleFactory(appConfig, moduleConfig, globalContext, stepContext);
      // TODO: appConfig being passed around too much

  // log.silly("*************************************************");
  // log.silly(`***moduleConfig[${++mxCnt}]: ${inspect(moduleConfig)}`);
	return module.exec();
}

function taskSuccess() {
  log.debug('Process finished');
  // this.emit(eventConsts.executeSuccess);
}

function taskFailed(err) {
  log.error(`Process failed: ${err}`);
  // this.emit(eventConsts.executeFailure, err);
}
