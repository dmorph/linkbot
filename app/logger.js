'use strict';

const {inspect: defaultInspect} = require('util');
// const inspect = require('eyes').inspector({maxLength: false});
const log = require('winston');
log.level = 'debug';
log.add(log.transports.File, { filename: "logs/client.log" });

module.exports = {log, inspect};

function inspect(obj) {
	return defaultInspect(obj, false, null);
}
