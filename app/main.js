'use strict';

const electron = require('electron');
const {app, autoUpdater, BrowserWindow} = electron;
const {ipcMain, Menu, Tray} = electron;
const io = require('socket.io-client');
const path = require('path');
const _ = require('lodash');
const fork = require('child_process').fork;

const SlfLinkServer = require("./slf-link/server");
const GlfProcessor = require("./generic-link/processor");
const {log, inspect} = require('./logger.js');
const appDefaults = require('./config/defaults');
const {config: appConsts, slf: slfConsts, glf: glfConsts} = appDefaults;

let mainWindow, linkConfig, trayMenu, tray;
let linkServerSocket, slfLink, glfLink;

log.info("Starting application");

process.on('uncaughtException', (err) => {
  log.error(`Uncaught error: ${err}`);
});

// var AutoLaunch = require('auto-launch');
// var autoStart = new AutoLaunch({
//   name: 'Glf Server',
//   path: process.execPath.match(/.*?\.app/)[0]
// });

// Quit when all windows are closed
app.on('window-all-closed', function() {
  mainWindow.hide();
});

// When application is ready, create tray window and
// contact link server for config
app.on('ready', function() {
  tray = new Tray(path.join(__dirname, appConsts.trayWaitIcon));
  trayMenu = Menu.buildFromTemplate(trayLoadingMenuTemplate);
  tray.setContextMenu(trayMenu);
  tray.setToolTip('LinkBot is starting ...');


  log.info(`Connecting to server for config: ${appConsts.serverUrl}`);
  linkServerSocket = io.connect(appConsts.serverUrl, {reconnect: true});
  linkServerSocket.on('config', (config) => {
    if (!config.isComplete) {
      log.warn("Retrieved server config is incomplete");
    }
    else {
      log.debug('Retrived config from server');
      log.silly(inspect(config));
        configureApp(config);
      }
  });
});

// TODO: Local settings override servers?
// ipcMain.on('close-settings-window', () => {
//     log.debug('Closing settings window');
//     if (mainWindow) {
//         mainWindow.hide();
//     }
// });

ipcMain.on('close-log-window', () => {
  if (mainWindow) {
    mainWindow.hide();
  }
});

function configureApp(config) {
  linkConfig = config;
  log.info('Server config set');

  log.info('Starting GLF process');
  configureGlfProcessor();
  log.info('Starting SLF process');
  configureSlfServer();
}

function configureGlfProcessor() {
  log.info('Configuring Glf');
  let glfConfig = linkConfig[glfConsts.name];
  glfConfig = _.merge({}, glfConsts, glfConfig);

  if (!glfLink) {
    log.info('Starting GLF');
    glfLink = new GlfProcessor(glfConfig);
    glfLink.init();
    glfLink.on(glfLink.eventConsts.invalidConfig, () => {
        log.warn(`GLF invalid config event thrown`);
    });
    // glfLink.on(glfLink.eventConsts.executeSuccess, () => {
    //     log.debug('GLF Link processed successfully');
    // });
    // glfLink.on(glfLink.eventConsts.executeFailure, () => {
    //     log.warn('Link process failed');  // Error mess
    // });
    glfLink.on(glfLink.eventConsts.slfLinkIn, (data) => {
        slf.processLinkIn(data);
    });
  }
}

function configureSlfServer() {
  log.info('Configuring SLF');
  let slfConfig = linkConfig[slfConsts.name];
  slfConfig = _.merge({}, slfConsts, slfConfig);

  if (!slfLink) {
    log.info('Starting SLF');
    try {
      log.silly(`slf config: ${inspect(slfConfig)}`);
      slfLink = new SlfLinkServer(slfConfig);
      slfLink.init();
      slfLink.on(slfLink.serverEvents.started, () => {
        log.debug("SLF server started");
        initTrayMenu();
      });
      slfLink.on(slfLink.serverEvents.invalidConfig, () => {
        log.warn(`SLF invalid config event thrown`);
      });
      slfLink.on(slfLink.serverEvents.linkOut, processSlfLinkOut);
    }
    catch(err) {
      log.error(err);
    }
  }
  else {
    slfLink.updateConfig(slfConfig);
  }
}

function setGlfConfig(config) {
  log.silly(config);
  linkConfig.glf = config;
  server.setConfig(linkConfig);
}

function processSlfLinkOut(linkEvent) {
  log.debug(`SLF linkout event: ${linkEvent.linkOutName}`);
  // TODO: does ssa expect LinkOutName??  Check prev code
  linkEvent['linkName'] = linkEvent.linkOutName;
  log.debug(`SLF event: ${inspect(linkEvent)}`);
  glfLink.executeModule(linkEvent);
      // .then( () => {
      //   log.debug(`${linkData.linkName} process successfully`);
      // })
      // .catch( () => {
      //   log.warn(`${linkData.linkName} process failed`);
      // });
}

function initTrayMenu() {
  // if (!isDevelopment) {
    trayMenuTemplate.push(trayQuitTemplate);
  // }

  trayMenu = Menu.buildFromTemplate(trayMenuTemplate);
  tray.setContextMenu(trayMenu);
  tray.setImage(path.join(__dirname, appConsts.trayIcon));
  tray.setToolTip('LinkBot is started');
  // tray.on doubleclick
  log.info("Initialised tray menu");
}

function loadWindow(displWindow, urlPath) {
  if (!displWindow) {
    displWindow = new BrowserWindow({
        name: "Link Bot",
        width: 400,
        height: 600,
        frame: false,
        toolbar: false,
        resizable: true
    });
  }

  displWindow.loadURL(`file://${__dirname}/public/${urlPath}`);
  displWindow.show();
  // displWindow.on('closed', () => {
  //     displWindow = null;
  // });

  // if (!isDevelopment) {
  //     displWindow.webContents.openDevTools({detach:true});
  // }
  return displWindow;
}

let trayQuitTemplate = {
  label: 'Quit',
  click: function () {
      app.quit();
  }
};

let trayLoadingMenuTemplate = [
  {
    label: appConsts.appTitle,
    enabled: false
  }
]
trayLoadingMenuTemplate.push(trayQuitTemplate);

let trayMenuTemplate = [
  {
    label: appConsts.appTitle,
    enabled: false
  },
  {
    label: 'Settings',
    click: function () {
      loadURL('settings.html');
    }
  },
  {
    label: 'Logs',
    click: function () {
      loadURL('logs.html');
    }
  }
];
