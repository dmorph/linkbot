const electron = require('electron');
const {ipcRenderer} = electron;
const {remote} = electron;
const path = require('path');

var closeEl = document.querySelector('.close');
closeEl.addEventListener('click', function (e) {
  ipcRenderer.send('close-settings-window');
});