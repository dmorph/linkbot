// App constants
module.exports = {
	config : {
		// Debug config
		socketPort : 3010,
		// "pathToLogFile": "C:\\Program Files (x86)\\Pitney Bowes\\Technology Links\\SSALink\\NodePackage\"
	},
	events : {
		server: {
			invalidConfig: "invalid-config",
			started: "started",
			linkOut: "link-out",
			// linkIn: "link-in"
		},
		// TODO: reset to ssa{ LinkOut,linkIn, socket}
		linkOut : {
			inputEvent: "notification",
			outputEvent: "notificationResponse",
			selectionEvent: "linkOutSelected"
		},
		linkIn : {   // TODO: rename ssaLinkOut
			triggerEvent: "linkInTriggered"
		},
		socket : {
			linkIn: "LinkIn",
			linkOut: "LinkOut"
		}
	},
	fields : {
		linkOut: {
			linkOutElementName: "linkOutName",
			bindingColumnElementName: "bindingColumn",
			targetAppName: "targetExternalApp"
		},
		linkIn: {
			linkInDataContainerElement: "linkInData",
			linkInIdentifier: "linkId",
			mapConfigElementName: "mapConfig",
			tableElementName: "table",
			statusElementName: "status",
			activeSelectionIdElementName: "activeSelectionId",
			bindingColumnLinkInElementName: "bindingColumn",
			selectionIdsElementName: "selectionIds"
		}
	},
	links : {

	}
}
