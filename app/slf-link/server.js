'use strict';

var util = require("util");
const EventEmitter = require("events").EventEmitter;
const express = require('express');
const io = require('socket.io');
const _ = require('lodash');

const {log, inspect} = require('../logger.js');
const {config: configConsts, events: eventConsts} = require('./defaults');

let appConfig, expressServer, socketServer, cnt=0;
let slfClientSocket, validConfig; //, linkInHashMapGlobal;

log.info("Starting slf-link server");

module.exports = SlfLinkServer;

// Inherit from EventEmitter
function SlfLinkServer(config) {
  EventEmitter.call(this);
  updateConfig(config);
}

util.inherits(SlfLinkServer, EventEmitter);

SlfLinkServer.prototype.updateConfig = updateConfig;
// SlfLinkServer.prototype.importXml = importXml;
SlfLinkServer.prototype.init = init;
SlfLinkServer.prototype.processLinkIn = processLinkIn;
SlfLinkServer.prototype.serverEvents = eventConsts.server;

function updateConfig(config) {
  log.debug("Updating SLF config");
  appConfig = _.merge({}, configConsts, config);
  log.silly(`Updated config: ${inspect(config)}`);
  validConfig = checkConfig();
  if (validConfig) {
    log.silly(`Set app port: ${appConfig.socketPort}, def port: ${configConsts.socketPort}`);
  }
}

function init() {
  log.debug('Sarting slf link server');

  if (!validConfig) {
    this.emit(eventConsts.server.invalidConfig);
    return;
  }

  // TODO: Refactor code pulled from previous slflink implementation.
  //This array will keep result resultSet as <tableName, linkOuts>
  var tableSpecificResult = [], resultToReturn = [];
  var linkOutInputEvent = eventConsts.linkOut.inputEvent;
  var linkOutOutputEvent = eventConsts.linkOut.outputEvent;
  var linkInTriggerEvent = eventConsts.linkIn.triggerEvent;

  var linkOutSelEvt = eventConsts.linkOut.selectionEvent;
  var socketConnectionTypeLinkIn = eventConsts.socket.linkIn;
  var socketConnectionTypeLinkOut = eventConsts.socket.linkOut;

  let port = appConfig.socketPort;
  let that = this;
  expressServer = express()
      .use((req, res) => res.sendFile(indexHtml))
      .listen(port, () => log.info(`SLF Links started listening on ${port}`));
  socketServer = io(expressServer);
  socketServer.sockets.on('connection', (socket) => {
    let queryLinkingType = socket.handshake.query.appLinkingType;
    log.debug(`Slf socket: new Connection, query: ${queryLinkingType}`);
    if (queryLinkingType === socketConnectionTypeLinkIn) {
        slfClientSocket = socket;
    }

    socket.on(linkOutInputEvent, (data) => {
      // TODO: processOutputEvent(data);
      var inputTableName = data;
      log.debug(`SLF inputTableName= ${inspect(inputTableName)} :${inputTableName.length}`);
      log.silly(``)
      var result_1 = [], indxTableName, indxLinkConfig;
      for (let k = 0; k < inputTableName.length; k++) {
        var newResultSet = {};
        indxTableName = inputTableName[k];
        indxLinkConfig = appConfig.linkOut[indxTableName];
        // log.debug(`InputTableName = ${indxTableName} :: ${inspect(indxLinkConfig)}`);
        if (appConfig.linkOut[indxTableName] != undefined) {  // .linkOut
          newResultSet.tableName = indxTableName;
          newResultSet.linkOuts = indxLinkConfig;
          result_1.push(newResultSet);
        }
      }
      // log. (`Intermediate Result = ${inspect(result_1)}`);

      resultToReturn = result_1;
      log.debug(`IF - resultToReturn ${inspect(resultToReturn)}`);

      //Emitting notification back;
      socket.emit(linkOutOutputEvent, resultToReturn);
    });

    //Place to receive an event from client - upon selection of a particular Linkout.
    socket.on(linkOutSelEvt, processLinkOut.bind(that));

    socket.on('error', (err) => {
      log.error(`SLF socket > ${inspect(err)}`);
    })
  });

  this.emit(eventConsts.server.started);
}

function checkConfig() {
  let valid = true;
  if (!appConfig.socketPort) {
    log.warn('Configuration is missing the server port');
  }
  else if (!appConfig.linkOut || !appConfig.linkIn) {
    log.warn('Configuration is missing the SLF link configuration');
    valid = false;
  }

  log.debug(`SLF server config valid: ${valid}`);
  return valid;
}

//Emitting notification back to GLF via main processor
function processLinkOut(linkData) {
  log.debug(`slf server selection event: ${inspect(linkData)}`);
  var staticArgsArr = linkData.staticArgs;

  log.debug("slf server.js --> Inside LinkoutSelection Event.. ");
  log.silly(`Process SLF LinkOut: ${inspect(linkData)}`);
  this.emit(eventConsts.server.linkOut, linkData);
}

// Sending link data to the SSA browser client
function processLinkIn(linkData) {
  log.debug(`SLF processing LinkIn: ${slfClientSocket} = ${slfClientSocket.connected}`);
  if (slfClientSocket && slfClientSocket.connected) {
    // linkInProcessor.emitLinkInDataAnalyst(linkInTriggerEvent, slfClientSocket,
    //         fileName, linkInHashMapGlobal);
    var linkInData = getMergedMetaqDataAndLinkInData(linkData)
    log.silly(`SSA sending SLF link-in: ${inspect(linkInData)}`);
    slfClientSocket.emit(linkInTriggerEvent, linkInData);
  }
  else {
    log.debug('Either analyst is not open or socket connection drop pls check');
  }
}

// TODO: Refactor code pulled from previous ssalink implementation.
function getMergedMetaqDataAndLinkInData(linkData) {
  var metadata = appConfig.linkIn[linkData.LinkDetails.LinkId];
  log.debug(`linkInMetadata -> ${metadata.bindColumn}: ${metadata.title}`);
  log.silly(`LinkInData: ${inspect(linkData)}`);

  var linkInData = {
    'linkId': linkData.LinkDetails.LinkId[0],
    'fileName': filename,
    'mapConfig': metadata.mapConfig,
    'table': metadata.table,
    'status': metadata.table != null ? 'FOUND' : 'NOT_FOUND',
    'bindingColumn': metadata.bindColumn,
    'title': metadata.title
  };
  var selectionIds = [];
  for (var i = 0; i < linkData.LinkDetails.SelectionIds[0].SelectionId.length; i++) {
    if(linkData.LinkDetails.SelectionIds[0].SelectionId[i] != undefined){
      if(linkData.LinkDetails.SelectionIds[0].SelectionId[i].trim().length>0){
        selectionIds.push(linkData.LinkDetails.SelectionIds[0].SelectionId[i]);
      }
    }
  }
  linkInData["selectionIds"] = selectionIds;
  log.silly(`linkInData: ${inspect(linkInData)}`);
  return linkInData;
}
