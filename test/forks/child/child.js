const logger = require('../logger.js');
console.log(`child created with PID: ${process.id}`);

process.on('exit', (code, signal) => {
	logger.log.info(`child exiting with code: ${code} and signal: ${signal}`)
});

process.on('message', (data) => {
	logger.log.silly(`child: got message from parent: ${data}`)
	process.send('hi, thanks for the message')
})