// const logger = require('winston');
// const Loggly = require('winston-loggly').Loggly;
// var loggly_options={ subdomain: "mysubdomain", inputToken: "efake000-000d-000e-a000-xfakee000a00" }
// logger.add(Loggly, loggly_options);

const log = require('winston');
log.level = 'silly';
log.add(log.transports.File, { filename: "logs/production.log" });

// const inspect = require('eyes').inspector({maxLength: false});
const defaultInspect = require("util");
const inspect = function(obj) {
	return defaultInspect(obj, false, null);
}

// module.exports=logger;
module.exports= {log, inspect};


// // http://stackoverflow.com/questions/13410754/i-want-to-display-the-file-name-in-the-log-statement
// // https://github.com/winstonjs/winston/issues/197
// http://stackoverflow.com/questions/11386492/accessing-line-number-in-v8-javascript-chrome-node-js
// // Return the last folder name in the path and the calling
// // module's filename.
// function getLabel() {
// 	console.log(`********** file: ${moduleFile}`);
//     var parts = moduleFile.split('/');
//     return parts[parts.length - 2] + '/' + parts.pop();
// };

// function logger(filePath) {
// 	moduleFile = filePath;
// 	return new winston.Logger({
//     	transports: [
//     		new winston.transports.Console({
//     			label: getLabel()
// 			}),
//     		new winston.transports.File({ 
//     			filename: "logs/linkserver.log",
//     			label: getLabel()
//     		})
//     	]
// 	});

// };

// let moduleFile;
// module.exports = {
// 	ExtraLog: logger, 
// 	inspect: inspect
// };