const logger = require('./logger.js');
var fork = require('child_process').fork;
var child = fork(__dirname + '/child/child.js')

child.on('message', (data) => {
	logger.log.info(`parent: got message from child: ${data}`)
})

child.send('hello from parent')